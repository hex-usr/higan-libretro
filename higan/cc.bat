@echo off
rem For use with Windows

mingw32-make -j6 binary=library target=libretro core=sfc
if not exist "out\higan_sfc_libretro.dll" (pause)

@echo on