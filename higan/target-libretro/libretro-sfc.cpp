//TODO: find a way to split the code into 4 linker objects without having
//icarus's Database variables complain of multiple definitions
//#include "libretro.hpp"

#include <sfc/sfc.hpp>
#undef platform

#include "resource/sfc.cpp"

enum class VideoMode {
  FullRes,
  MergeScanlines,
  HalfRes,
  FullResCrop,
  MergeScanlinesCrop,
  HalfResCrop
};
static VideoMode video_mode = VideoMode::FullRes;
static uint sgb_version;

auto adjust_video_resolution(uint& width, uint& height, uint& pitch, float& pixel_aspect_correction) -> uint {
  uint old_height = height;

  if(video_mode == VideoMode::HalfRes
  || video_mode == VideoMode::HalfResCrop
  ) {
    width = 256;
  }

  if(video_mode == VideoMode::MergeScanlinesCrop
  || video_mode == VideoMode::HalfResCrop
  ) {
    old_height = 240;
  }

  if(video_mode == VideoMode::MergeScanlines
  || video_mode == VideoMode::MergeScanlinesCrop
  || video_mode == VideoMode::HalfRes
  || video_mode == VideoMode::HalfResCrop
  ) {
    if(height > 240) pitch <<= 1;
    height = 240;
  }

       if(width == 512 && height <= 240) pixel_aspect_correction = 0.5f;
  else if(width == 256 && height >  240) pixel_aspect_correction = 2.0f;
  else                                   pixel_aspect_correction = 1.0f;

  if(video_mode == VideoMode::FullResCrop
  || video_mode == VideoMode::MergeScanlinesCrop
  || video_mode == VideoMode::HalfResCrop
  ) {
    height = height == 240 ? 224 : 448;
    return pitch * ((old_height - height) >> 1);
  }

  return 0;
}

auto video_output(const uint32* data, uint width, uint height, uint pitch) -> void {
  if(video_mode == VideoMode::FullRes
  || video_mode == VideoMode::FullResCrop
  || video_mode == VideoMode::MergeScanlines
  || video_mode == VideoMode::MergeScanlinesCrop
  ) {
    video_cb(data, width, height, pitch);
    return;
  }

  uint word_pitch = pitch >> 2;

  retro_framebuffer fb = {};
  fb.width = width;
  fb.height = height;
  fb.access_flags = RETRO_MEMORY_ACCESS_WRITE;

  uint dst_pitch;
  void* dst_buffer;

  //Use SOFTWARE_FRAMEBUFFER to avoid an extra copy if possible.
  if(environ_cb(RETRO_ENVIRONMENT_GET_CURRENT_SOFTWARE_FRAMEBUFFER, &fb) && fb.format == RETRO_PIXEL_FORMAT_XRGB8888) {
    dst_buffer = fb.data;
    dst_pitch = fb.pitch;
  } else {
    static uint32 scratch[256 * 240];
    dst_buffer = scratch;
    dst_pitch = width * sizeof(uint32);
  }

  auto* dst = static_cast<uint32*>(dst_buffer);
  auto dst_pitch_word = dst_pitch >> 2;
  for(uint y = 0; y < height; y++, dst += dst_pitch_word, data += word_pitch) {
    for(uint x = 0, src_x = 0; x < width; x++, src_x += 2) dst[x] = data[src_x];
  }

  video_cb(dst_buffer, width, height, dst_pitch);
}

auto flush_variables() -> void {
  retro_variable variable = {"higan_sfc_internal_resolution", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
         if(strcmp(variable.value, "512x480") == 0) video_mode = VideoMode::FullRes;
    else if(strcmp(variable.value, "512x448") == 0) video_mode = VideoMode::FullResCrop;
    else if(strcmp(variable.value, "512x240") == 0) video_mode = VideoMode::MergeScanlines;
    else if(strcmp(variable.value, "512x224") == 0) video_mode = VideoMode::MergeScanlinesCrop;
    else if(strcmp(variable.value, "256x240") == 0) video_mode = VideoMode::HalfRes;
    else if(strcmp(variable.value, "256x224") == 0) video_mode = VideoMode::HalfResCrop;
  }

  variable = {"higan_sfc_fast_ppu", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Hacks/FastPPU/Enable", strcmp(variable.value, "ON") == 0);
  }

  variable = {"higan_sfc_no_sprite_limit", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Hacks/FastPPU/NoSpriteLimit", strcmp(variable.value, "ON") == 0);
  }

  variable = {"higan_sfc_hires_mode_7", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Hacks/FastPPU/HiresMode7", strcmp(variable.value, "ON") == 0);
  }

  variable = {"higan_sfc_fast_dsp", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Hacks/FastDSP/Enable", strcmp(variable.value, "ON") == 0);
  }

  variable = {"higan_sfc_coprocessors_delayed_sync", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Hacks/Coprocessors/DelayedSync", strcmp(variable.value, "ON") == 0);
  }

  variable = {"higan_sfc_color_emulation", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Video/ColorEmulation", strcmp(variable.value, "ON") == 0);
    Emulator::video.setPalette();
  }

  variable = {"higan_sfc_blur_emulation", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    emulator->configure("Video/BlurEmulation", strcmp(variable.value, "ON") == 0);
  }

  variable = {"higan_sfc_sgb_version", nullptr};
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &variable) && variable.value) {
    sgb_version = string{variable.value}.natural();
  }
}

auto check_variables() -> void {
  bool updated = false;
  if(environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated) flush_variables();
}

auto create_emulator_interface() -> Emulator::Interface* {
  auto* interface = new SuperFamicom::Interface;
  interface->set("Fast PPU", false);
  interface->set("Fast PPU/No Sprite Limit", false);
  interface->set("Fast PPU/Hires Mode 7", false);
  interface->set("Fast DSP", false);
  interface->set("Blur Emulation", false);
  return interface;
}

auto get_special_id_from_path(uint default_id, const char* path) -> uint {
  if(!path) return default_id;

  string p = path;
  if(p.endsWith(".gb") || p.endsWith(".gbc")) return SuperFamicom::ID::GameBoy;

  return default_id;
}

auto get_default_id_extension(uint id) -> string {
  //Super Game Boy can take .gbc as well if the game is dual-mode.
  //This path is only relevant as a fallback
  //in the rare case that we don't know the path.
  switch(id) {
  case SuperFamicom::ID::SuperFamicom: return ".sfc";
  case SuperFamicom::ID::GameBoy:      return ".gb";
  default: return "";
  }
}

auto load_special_bios(uint id) -> bool {
  if(id == 2) {  //Game Boy
    //Find the Super Game Boy ROM here.
    libretro_print(RETRO_LOG_INFO, "Trying to find Super Game Boy BIOS in system directories.\n");
    string path = locate_libretro({"Super Game Boy", sgb_version == 1 ? "" : " 2", ".sfc/program.rom"});

    if(!file::exists(path)) {
      libretro_print(RETRO_LOG_ERROR, "Unable to find Super Game Boy BIOS.\n");
      return false;
    }

    program->game_paths(SuperFamicom::ID::SuperFamicom) = Location::dir(path);
    program->loaded_manifest(SuperFamicom::ID::SuperFamicom) =
      plain_icarus.manifest(program->game_paths(SuperFamicom::ID::SuperFamicom));

    if(!program->loaded_manifest(SuperFamicom::ID::SuperFamicom)) {
      libretro_print(RETRO_LOG_ERROR, "Could not create manifest for Super Game Boy BIOS.\n");
      return false;
    }

    return true;
  } else if(id == 3) {  //BS Memory
  } else if(id == 4) {  //Sufami Turbo A
  } else if(id == 5) {  //Sufami Turbo B
  }
  return true;
}

#define RETRO_DEVICE_JOYPAD_MULTITAP       RETRO_DEVICE_SUBCLASS(RETRO_DEVICE_JOYPAD, 0)
#define RETRO_DEVICE_LIGHTGUN_SUPER_SCOPE  RETRO_DEVICE_SUBCLASS(RETRO_DEVICE_LIGHTGUN, 0)
#define RETRO_DEVICE_LIGHTGUN_JUSTIFIER    RETRO_DEVICE_SUBCLASS(RETRO_DEVICE_LIGHTGUN, 1)
#define RETRO_DEVICE_LIGHTGUN_JUSTIFIERS   RETRO_DEVICE_SUBCLASS(RETRO_DEVICE_LIGHTGUN, 2)

auto set_environment_info(retro_environment_t cb) -> void {
  //TODO: Hook up RETRO_ENVIRONMENT_SET_SUBSYSTEM_INFO for Sufami/BSX/SGB?
  //IIRC, no known frontend actually hooks it up properly, so doubt there is any
  //real need for now.

  static const retro_controller_description port_1[] = {
    { "SNES Joypad", RETRO_DEVICE_JOYPAD },
    { "SNES Mouse", RETRO_DEVICE_MOUSE },
  };

  static const retro_controller_description port_2[] = {
    { "SNES Joypad", RETRO_DEVICE_JOYPAD },
    { "SNES Mouse", RETRO_DEVICE_MOUSE },
    { "Multitap", RETRO_DEVICE_JOYPAD_MULTITAP },
    { "SuperScope", RETRO_DEVICE_LIGHTGUN_SUPER_SCOPE },
    { "Justifier", RETRO_DEVICE_LIGHTGUN_JUSTIFIER },
    { "Justifiers", RETRO_DEVICE_LIGHTGUN_JUSTIFIERS },
  };

  static const retro_controller_info ports[] = {
    { port_1, 2 },
    { port_2, 6 },
    { 0 },
  };

  cb(RETRO_ENVIRONMENT_SET_CONTROLLER_INFO, const_cast<retro_controller_info*>(ports));

  #define port(id) \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_LEFT,   "D-Pad Left" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_UP,     "D-Pad Up" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_DOWN,   "D-Pad Down" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_RIGHT,  "D-Pad Right" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_B,      "B" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_A,      "A" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_X,      "X" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_Y,      "Y" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L,      "L" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R,      "R" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_SELECT, "Select" }, \
    { id, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_START,  "Start" } \

  static const retro_input_descriptor desc[] = {
    port(0),
    port(1),
    port(2),
    port(3),
    port(4),

    { 0 },
  };

  cb(RETRO_ENVIRONMENT_SET_INPUT_DESCRIPTORS, const_cast<retro_input_descriptor*>(desc));

  static const retro_variable vars[] = {
    { "higan_sfc_internal_resolution", "Internal resolution; 512x480|512x448|512x240|512x224|256x240|256x224" },
    { "higan_sfc_fast_ppu", "Fast PPU; ON|OFF" },
    { "higan_sfc_no_sprite_limit", "Disable Sprite Limit (Fast PPU only); OFF|ON" },
    { "higan_sfc_hires_mode_7", "Hires Mode 7 (Fast PPU only); OFF|ON" },
    { "higan_sfc_fast_dsp", "Fast DSP; ON|OFF" },
    { "higan_sfc_coprocessors_delayed_sync", "Fast coprocessors (delayed sync); ON|OFF" },
    { "higan_sfc_color_emulation", "Color emulation; OFF|ON" },
    { "higan_sfc_blur_emulation", "Blur emulation; OFF|ON" },
    { "higan_sfc_sgb_version", "Preferred Super Game Boy version (restart); 1|2" },
    { nullptr },
  };
  cb(RETRO_ENVIRONMENT_SET_VARIABLES, const_cast<retro_variable*>(vars));
}

//Normally, these files are loaded from an external folder after higan is installed,
//but a libretro core shouldn't require that for anything but the Game Boy Advance BIOS.
//If any of these files are changed, please run sourcery inside the resources/ folder.
auto load_builtin_system_file(string name) -> vfs::shared::file {
  if(name == "boards.bml") {
    return vfs::memory::file::open(Resource::System::Boards, sizeof(Resource::System::Boards));
  }

  if(name == "ipl.rom") {
    return vfs::memory::file::open(Resource::System::IPLROM, sizeof(Resource::System::IPLROM));
  }

  return {};
}

auto Program::inputPoll(uint port, uint device, uint input) -> int16 {
  poll_once();

  //TODO: This will need to be remapped on a per-system basis.
  uint libretro_port;
  uint libretro_id;
  uint libretro_device;
  uint libretro_index = 0;

  static const uint joypad_mapping[] = {
    RETRO_DEVICE_ID_JOYPAD_UP,
    RETRO_DEVICE_ID_JOYPAD_DOWN,
    RETRO_DEVICE_ID_JOYPAD_LEFT,
    RETRO_DEVICE_ID_JOYPAD_RIGHT,
    RETRO_DEVICE_ID_JOYPAD_B,
    RETRO_DEVICE_ID_JOYPAD_A,
    RETRO_DEVICE_ID_JOYPAD_Y,
    RETRO_DEVICE_ID_JOYPAD_X,
    RETRO_DEVICE_ID_JOYPAD_L,
    RETRO_DEVICE_ID_JOYPAD_R,
    RETRO_DEVICE_ID_JOYPAD_SELECT,
    RETRO_DEVICE_ID_JOYPAD_START,
  };

  static const uint mouse_mapping[] = {
    RETRO_DEVICE_ID_MOUSE_X,
    RETRO_DEVICE_ID_MOUSE_Y,
    RETRO_DEVICE_ID_MOUSE_LEFT,
    RETRO_DEVICE_ID_MOUSE_RIGHT,
  };

  switch(port) {
  case SuperFamicom::ID::Port::Controller1: libretro_port = 0; break;
  case SuperFamicom::ID::Port::Controller2: libretro_port = 1; break;

  default:
    return 0;
  }

  switch(device) {
  case SuperFamicom::ID::Device::Gamepad:
    libretro_device = RETRO_DEVICE_JOYPAD;
    libretro_id = joypad_mapping[input];
    break;

  case SuperFamicom::ID::Device::Mouse:
    libretro_device = RETRO_DEVICE_MOUSE;
    libretro_id = mouse_mapping[input];
    break;

  case SuperFamicom::ID::Device::SuperMultitap:
    libretro_device = RETRO_DEVICE_JOYPAD;  //Maps to player [2, 5].
    libretro_port += input / 12;
    libretro_id = joypad_mapping[input % 12];
    break;

  //TODO: Super Scope/Justifiers.
  //higan enforces relative cursor movement, which is deprecated in libretro
  //in favor of absolute cursor movement.

  default:
    return 0;
  }

  return input_state(libretro_port, libretro_device, libretro_index, libretro_id);
}

auto set_controller_ports(unsigned port, unsigned device) -> void {
  switch(device) {
  default:
  case RETRO_DEVICE_NONE:                 device = SuperFamicom::ID::Device::None; break;
  case RETRO_DEVICE_JOYPAD:               device = SuperFamicom::ID::Device::Gamepad; break;
  case RETRO_DEVICE_ANALOG:               device = SuperFamicom::ID::Device::Gamepad; break;
  case RETRO_DEVICE_JOYPAD_MULTITAP:      device = SuperFamicom::ID::Device::SuperMultitap; break;
  case RETRO_DEVICE_MOUSE:                device = SuperFamicom::ID::Device::Mouse;
  case RETRO_DEVICE_LIGHTGUN_SUPER_SCOPE: device = SuperFamicom::ID::Device::SuperScope; break;
  case RETRO_DEVICE_LIGHTGUN_JUSTIFIER:   device = SuperFamicom::ID::Device::Justifier; break;
  case RETRO_DEVICE_LIGHTGUN_JUSTIFIERS:  device = SuperFamicom::ID::Device::Justifiers; break;
  }
  if(port < 2) emulator->connect(port, device);
}

/*
auto setup_memory_map() -> void {
  vector<retro_memory_descriptor> descriptors;
  retro_memory_descriptor descriptor;

  descriptor = {};  //00-3f,80-bf:0000-1fff
  descriptor.flags      = 0;
  descriptor.ptr        = SuperFamicom::cpu.wram;
  descriptor.offset     = 0x0;
  descriptor.start      = 0x00'0000;
  descriptor.select     = 0x40'e000;
  descriptor.disconnect = 0xff'e000;
  descriptor.len        = 0x2000;
  descriptor.addrspace  = "WRAM";
  descriptors.append(descriptor);

  descriptor = {};  //7e-7f:0000-ffff
  descriptor.flags      = 0;
  descriptor.ptr        = SuperFamicom::cpu.wram;
  descriptor.offset     = 0x0;
  descriptor.start      = 0x7e'0000;
  descriptor.select     = 0xfe'0000;
  descriptor.disconnect = 0xfe'0000;
  descriptor.len        = 0x2000;
  descriptor.addrspace  = "WRAM";
  descriptors.append(descriptor);
}
*/

auto get_memory_data(unsigned id) -> void* {
  switch(id) {

  case RETRO_MEMORY_SAVE_RAM: {
         if(SuperFamicom::cartridge.has.SA1)        return SuperFamicom::sa1.bwram.data();
    else if(SuperFamicom::cartridge.has.SuperFX)    return SuperFamicom::superfx.ram.data();
    else if(SuperFamicom::cartridge.has.HitachiDSP) return SuperFamicom::hitachidsp.ram.data();
    else if(SuperFamicom::cartridge.has.SPC7110)    return SuperFamicom::spc7110.ram.data();
    else if(SuperFamicom::cartridge.has.OBC1)       return SuperFamicom::obc1.ram.data();

    if(SuperFamicom::cartridge.ram.size()) {
      return SuperFamicom::cartridge.ram.data();
    } else if(SuperFamicom::cartridge.has.NECDSP
    && SuperFamicom::necdsp.revision == SuperFamicom::NECDSP::Revision::uPD96050) {
      return SuperFamicom::necdsp.dataRAM;
    }
  }

  case RETRO_MEMORY_SYSTEM_RAM: {
    return SuperFamicom::cpu.wram;
  }

  }

  return nullptr;
}

auto get_memory_size(unsigned id) -> size_t {
  switch(id) {

  case RETRO_MEMORY_SAVE_RAM: {
         if(SuperFamicom::cartridge.has.SA1)        return SuperFamicom::sa1.bwram.size();
    else if(SuperFamicom::cartridge.has.SuperFX)    return SuperFamicom::superfx.ram.size();
    else if(SuperFamicom::cartridge.has.HitachiDSP) return SuperFamicom::hitachidsp.ram.size();
    else if(SuperFamicom::cartridge.has.SPC7110)    return SuperFamicom::spc7110.ram.size();
    else if(SuperFamicom::cartridge.has.OBC1)       return SuperFamicom::obc1.ram.size();

    if(SuperFamicom::cartridge.ram.size()) {
      return SuperFamicom::cartridge.ram.size();
    } else if(SuperFamicom::cartridge.has.NECDSP
    && SuperFamicom::necdsp.revision == SuperFamicom::NECDSP::Revision::uPD96050) {
      return 2048 * sizeof(uint16);
    }
  }

  case RETRO_MEMORY_SYSTEM_RAM: {
    return 128 * 1024;
  }

  }

  return 0;
}

const char* BackendSpecific::extensions = "sfc|smc|gb|gbc|bml|rom";  //icarus supports headered ROMs as well.
const char* BackendSpecific::game_type = "sfc";
const char* BackendSpecific::name = "higan (Super Famicom)";
const double BackendSpecific::audio_rate = 44100.0;  //MSU1 is 44.1k CD, so use that.
